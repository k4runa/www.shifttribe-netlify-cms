import React from 'react'
import PropTypes from 'prop-types'
import Content, { HTMLContent } from '../components/Content'

import PostHeading from '../components/PostHeading'

export const CoursePageTemplate = ({ title, content, contentComponent }) => {
  const PageContent = contentComponent || Content

  return (
    <div className="page-template page-courses">
      <div className="site-wrap">
        <section className="post-wrap is-image">
          <div className="top-wrap">
            <PostHeading title={title} background={'/assets/images/courses-heading.jpg'}/>
          </div>
          <div className="post-content flex-wrap flex-container" style={{ paddingBottom: '6em' }}>
            <div className="column">
              <PageContent className="content" content={content} />
            </div>
            <div className="column">
              <div className="card">
                <img src="/assets/images/mindful-book-thumbnail.jpg" />
                <div className="card-content">
                  <p>Purchase your <b><em>Essentials of mindfulness</em></b> ebook and transform your life today. (<a href="https://www.amazon.com/Essentials-Mindfulness-Concepts-Empowered-Living-ebook/dp/B07D9RJ4LW/ref=sr_1_6?ie=UTF8&qid=1527673841&sr=8-6&keywords=essentials+of+mindfulness">Amazon</a>)</p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

CoursePageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
}

const CoursePage = ({ data }) => {
  const { markdownRemark: post } = data

  return (
    <CoursePageTemplate
      contentComponent={HTMLContent}
      title={post.frontmatter.title}
      content={post.html}
    />
  )
}

CoursePage.propTypes = {
  data: PropTypes.object.isRequired,
}

export default CoursePage

export const aboutPageQuery = graphql`
  query CoursePage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
      }
    }
  }
`
