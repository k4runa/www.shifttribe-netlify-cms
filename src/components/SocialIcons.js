import React, { Fragment } from 'react'

const SocialIcons = () =>
<Fragment>
  <a href="https://www.facebook.com/ShiftSA/"><i className="facebook"></i></a>
  <a href="https://www.youtube.com/channel/UC8J9zGkYcyt0-wCG2Z9ZZUg"><i className="youtube"></i></a>
  <a href="https://www.instagram.com/shiftsa/"><i className="instagram"></i></a>
  {/* <a href="#"><i className="vimeo"></i></a> */}
</Fragment>

export default SocialIcons;
