---
templateKey: blog-post
title: What is mindfulness and why?
date: 2018-06-16T12:21:58.000Z
author: Chloe Guilhermino
description: The practice of mindfulness if one of the most powerful tools we have available to us today that allows us to become conscious of ourselves as the creators of our own lives.
tags:
  - mindfulness
---

The practice of mindfulness if one of the most powerful tools we have available to us today that allows us to become conscious of ourselves as the creators of our own lives.

The term mindfulness is thrown around a lot today but do we actually understand what it is and how to apply it into our everyday life to bring the deep transformation and empowerment we all so desire?
Mindfulness is the practise of self-awareness. The continuous awareness of one’s own thoughts, words and actions on a moment to moment basis as best you can.

![What is Mindfulness](/assets/images/blog-what-is-mindfullness.jpg)

Why is this important? In the practice of mindfulness we are in the awareness that it is through our own thoughts words and actions that our lives are created. It is we are creating our own lives on a moment to moment basis and yet we so often think, say and do things that are not serving us or the world around us.

A large proportion of the world today is living very unconsciously, unaware of the power they possess to create a magnificent life and world for themselves. These people are still very much in a perception that life is happening to them and that they have no power over their lives and the outcome.

Mindfulness is the complete opposite of this. In mindful practice we become aware that it is us who is creating our lives and that life is not happening to us but rather through us. Our lives are the reflection of what is within us.

So when we become aware of this incredible power and potential of ourselves as the creators of our lives, we become empowered to create and live lives that are in true alignment with who we really are and what we prefer.

The practice of mindfulness is perhaps the greatest passion of myself, Chloe Guilhermino and my husband Ashley Epstein - as we have had the first-hand experience of how it has transformed our own lives and sense of self.

We wish to share this with as many people as we can and so have written a book and designed an online course – The Essentials of Mindfulness. So if this is something you are interested in learning more about. You can purchase both the book and course through our website.

We are also available on our facebook page and email for questions absolutely anytime.

We highly encourage you to look more into the practice of mindfulness and bring it into your life as best as you can.

Much love
Shift Tribe
