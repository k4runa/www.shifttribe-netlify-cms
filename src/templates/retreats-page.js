import React from 'react'
import PropTypes from 'prop-types'
import Content, { HTMLContent } from '../components/Content'

import PostHeading from '../components/PostHeading'

export const RetreatsPageTemplate = ({ title, content, contentComponent }) => {
  const PageContent = contentComponent || Content

  return (
    <div className="page-template page-about">
      <div className="site-wrap">
        <section className="post-wrap is-image">
          <div className="top-wrap">
          <PostHeading title={title} background={'/assets/images/retreats-heading.jpg'} />
          </div>
          <div className="post-content" style={{ paddingBottom: '6em' }}>
            <PageContent className="content" content={content} />
          </div>
        </section>
      </div>
    </div>
  )
}

RetreatsPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
}

const RetreatsPage = ({ data }) => {
  const { markdownRemark: post } = data

  return (
    <RetreatsPageTemplate
      contentComponent={HTMLContent}
      title={post.frontmatter.title}
      content={post.html}
    />
  )
}

RetreatsPage.propTypes = {
  data: PropTypes.object.isRequired,
}

export default RetreatsPage

export const retreatPageQuery = graphql`
  query RetreatsPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
      }
    }
  }
`
