import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'

export default class IndexPage extends React.Component {
  render() {
    const { data } = this.props
    const { edges: posts } = data.allMarkdownRemark

    return (
      <div className="home-template">
        <div className="site-wrap">

        {/* start featured */}
        <div className="top-wrap">
        	<section className="title-wrap">
        		<div className="top-social">
        			<a href="https://www.facebook.com/ShiftSA/"><i className="facebook"></i></a>
        			<a href="https://www.youtube.com/channel/UC8J9zGkYcyt0-wCG2Z9ZZUg"><i className="youtube"></i></a>
              <a href="https://www.instagram.com/shiftsa/"><i className="instagram"></i></a>
              {/* <a href="#"><i className="vimeo"></i></a> */}
        		</div>
        		<div className="is-featured bg-top is-cover" style={{ backgroundImage: `url(/assets/images/shift-beach.jpg)` }}>
        			<h1 className="title">Join the Tribe.</h1>
        			<h2 className="description">Shift is a growing global organisation which strives to achieve world peace through inner peace.</h2>
        		</div>
        		<div className="featured-wrap">
        			<div className="bg-medium" data-sr-id="2" style={{ visibility: `visible`,  WebkitTransform: `translateY(0) scale(1)`, opacity: `1`, transform: `translateY(0) scale(1)`, opacity: `1`, WebkitTransition: `-webkit-transform 0.65s ease-in-out 0s, opacity 0.65s ease-in-out 0s`, transition: `transform 0.65s ease-in-out 0s, opacity 0.65s ease-in-out 0s` }}>
        				<div className="vertical text-featured">
        					<div className="text rotate">
        						Featured
        					</div>
        				</div>
        				<div className="featured-flex" data-sr-id="1" style={{ visibility: `visible`,  WebkitTransform: `translateY(0) scale(1)`, opacity: `1`, transform: `translateY(0) scale(1)`, opacity: `1`, WebkitTransition: `-webkit-transform 0.65s ease-in-out 0s, opacity 0.65s ease-in-out 0s`, transition: `transform 0.65s ease-in-out 0s, opacity 0.65s ease-in-out 0s` }}>
        					<div className="left first">
        						<div className="appear">
        							<a href="/blog/2018-05-04-the-essentials-of-mindfullness"><div className="featured-image featured-home-image" style={{ backgroundImage: `url(/assets/images/mindful-book-cover.jpg)` }}></div></a>
        						</div>
        					</div>
        					<div className="right first">
        						<article className="featured-post-wrap">
        							<div className="featured-post">
        								<h2><a href="/blog/2018-05-04-the-essentials-of-mindfullness">The Essentials of Mindfulness</a></h2>
        								<div className="item-excerpt">
        									<p>Learn about your mind and life in a new way, today! In this book, you’ll embark on a journey with us as we explore the mechanics of the mind and mindfulness, and how it can help you to expand your potential, making you more aware of the power that you possess as an individual. <a href="/the-essential-of-mindfulness/">[...]</a></p>
        								</div>
        								<a href="/blog/2018-05-04-the-essentials-of-mindfullness"><span className="featured-more-button">Read more</span></a>
        							</div>
        						</article>
        					</div>
        				</div>
        			</div>
        			<div className="bg-bottom" data-sr-id="3" style={{ visibility: `visible`,  WebkitTransform: `translateY(0) scale(1)`, opacity: `1`, transform: `translateY(0) scale(1)`, opacity: `1`, WebkitTransition: `-webkit-transform 0.65s ease-in-out 0s, opacity 0.65s ease-in-out 0s`, transition: `transform 0.65s ease-in-out 0s, opacity 0.65s ease-in-out 0s` }}></div>
        		</div>
        		<div className="is-featured vertical text-posts">
        			<div className="text rotate">
        				Articles
        			</div>
        		</div>
        	</section>
        </div>
        {/* end featured */}

          <section className="section">
            <div className="container news-wrap" style={{ paddingTop: '6em', paddingBottom: '6em' }}>
              {posts
                .map(({ node: post }) => (
                  <div
                    className="content news-card"
                    style={{ border: '1px solid #eaecee', padding: '2em 4em', margin: '2em 0 2em 0' }}
                    key={post.id}
                  >
                    <p>
                      <Link className="" to={post.fields.slug}>
                        <b>{post.frontmatter.title}</b>
                      </Link>
                      <span> &bull; </span>
                      <small>{post.frontmatter.date}</small>
                    </p>
                    <p>
                      {post.excerpt}
                      <br />
                      <br />
                      <Link className="button is-small read-more" to={post.fields.slug}>
                        Keep Reading →
                      </Link>
                    </p>
                  </div>
                ))}
            </div>
          </section>
        </div>
      </div>
    )
  }
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array,
    }),
  }),
}

export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] },
      filter: { frontmatter: { templateKey: { eq: "blog-post" } }}
    ) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          fields {
            slug
          }
          frontmatter {
            title
            templateKey
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`
