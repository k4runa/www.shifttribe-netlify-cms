import React from 'react'
import Link from 'gatsby-link'

import SocialIcons from './SocialIcons';

const PostHeading = ({ title, background }) => (
  <div className="top-wrap">
    <section className="title-wrap">
      <div className="top-social">
        <SocialIcons />
      </div>
      <div className="bg-top">
        <h1 className="title">{title}</h1>
      </div>
      <div className="featured-post-image" style={{ backgroundImage: `url(${background})` }}></div>
    </section>
  </div>
)

export default PostHeading
