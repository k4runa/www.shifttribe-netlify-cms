---
templateKey: 'about-page'
path: /about
title: Our Values
---
### We believe in the Power of Collaboration & Co-Creation.

We aspire to facilitate a link between the teachers and the learners of this world by organizing creative & interactive Events, Workshops & Festivals where all may come together to Share, Learn & Grow in a supported and conscious space.

![DSC_9523](/assets/images/about-us-meditation.jpg)

There is something extraordinary about people coming together for a mass yoga or meditation session – there is a tingling in the air. If each individual finds peace within themselves, we are bound to become a peaceful society.

Each year we get together for a large beach meditation at Camps Bay.

<iframe width="100%" height="400" src="https://www.youtube.com/embed/EGe1LXn7lGU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
