import React from 'react'
import Link from 'gatsby-link'

const Subscribe = () => (
  <div className="subscribe-wrap">
	<div className="vertical text-newsletter">
		<div className="text rotate">
			Newsletter
		</div>
	</div>
	<div className="subscribe">
		<div className="gh-subscribe">
			<div className="text-subscribe">Subscribe to our <b>newsletter</b></div>
      <form
        action="https://shifttribe.us18.list-manage.com/subscribe/post?u=05147a7a28343fc5cb5ea8329&amp;id=4a7017c25f"
        method="post"
        id="mc-embedded-subscribe-form"
        name="mc-embedded-subscribe-form"
        className="validate"
        target="_blank"
        noValidate
      >
          {/* form input */}
          <div className="form-group">
            <input
              className="subscribe-email"
              type="email"
              name="EMAIL"
              placeholder="Your email address"
              id="mce-EMAIL"
            />
          </div>
          <div id="mce-responses" className="clear">
            <div className="response" id="mce-error-response" style={{ display: 'none' }}></div>
            <div className="response" id="mce-success-response" style={{ display:'none' }}></div>
          </div>

          {/* real people should not fill this in and expect good things
             - do not remove this or risk form bot signups */}
          <div style={{ position: 'absolute', left: '-5000px' }} aria-hidden="true">
            <input
              type="text"
              name="b_05147a7a28343fc5cb5ea8329_4a7017c25f"
              tabIndex="-1"
              value=""
            />
          </div>

          {/* form submit */}
          <button
            className=""
            type="submit"
            value="Subscribe"
            name="subscribe"
            id="mc-embedded-subscribe"
          >
            <span>Subscribe</span>
          </button>

      </form>
		</div>
	</div>
</div>
)

export default Subscribe
