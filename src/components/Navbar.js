import React from 'react'
import Link from 'gatsby-link'

import github from '../img/github-icon.svg'
import logo from '../img/logo.svg'

const Navbar = () => (
  <div className="site-wrap">
    <header className="header-wrap">
    	<div className="left">
    		<div className="logo">
    			<Link className="logo-image" to="/">
            <img src="/assets/logo/shift--logo.png" alt="Shift" />
          </Link>
    		</div>
    	</div>
    	<div className="right">
    		<nav className="nav-wrap">
    			<label htmlFor="toggle" className="nav-wrap--label hamburger hamburger--minus">
    				<span className="hamburger-box">
    					<span className="hamburger-inner"></span>
    				</span>
    			</label>
    			<input type="checkbox" id="toggle" className="nav-wrap--toggle" />
    			<ul className="nav-wrap--list">
    				<li className="nav-wrap--list-item ">
              <Link to="/" className="nav-wrap--link">Home</Link>
            </li>
            <li className="nav-wrap--list-item ">
              <Link to="/about/" className="nav-wrap--link">About</Link>
            </li>
            <li className="nav-wrap--list-item ">
              <Link to="/retreats/" className="nav-wrap--link">Retreats</Link>
            </li>
            <li className="nav-wrap--list-item ">
              <Link to="/courses/" className="nav-wrap--link">Courses</Link>
            </li>
    			</ul>
    		</nav>
    	</div>
    </header>
  </div>
)

export default Navbar
