import React from 'react'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'

import BlogHeading from '../components/BlogHeading'

class TagRoute extends React.Component {
  render() {
    const posts = this.props.data.allMarkdownRemark.edges
    const postLinks = posts.map(post => (
      <li key={post.node.fields.slug}>
        <Link to={post.node.fields.slug}>
          <b>{post.node.frontmatter.title}</b>
        </Link>
      </li>
    ))
    const tag = this.props.pathContext.tag
    const title = this.props.data.site.siteMetadata.title
    const totalCount = this.props.data.allMarkdownRemark.totalCount
    const tagHeader = `${totalCount} post${
      totalCount === 1 ? '' : 's'
    } tagged with “${tag}”`

    return (
      <section className="tag-template">
        <Helmet title={`${tag} | ${title}`} />
        <div className="site-wrap">
          <div className="top-wrap">
            <BlogHeading title={tagHeader} />
          </div>
          <div className="columns" style={{ paddingBottom: '6em' }}>
            <ol className="taglist">{postLinks}</ol>
            <p className="tags">
              <Link to="/tags/">Browse all tags</Link>
            </p>
          </div>
        </div>
      </section>
    )
  }
}

export default TagRoute

export const tagPageQuery = graphql`
  query TagPage($tag: String) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      limit: 1000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  }
`
