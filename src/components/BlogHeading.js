import React from 'react'
import Link from 'gatsby-link'

import SocialIcons from './SocialIcons';

const Author = ({ author, date }) => (
  <div className="item-meta author-name">
    <em>~ &nbsp;</em>
    <a href="/author/daniel/">{author}</a>
    <em>&nbsp;~</em>
    <br/>
    <time datetime="{date}"> Published on {date}</time>
    <br/>
  </div>
)

const BlogHeading = ({ title, author, date }) => (
  <div className="top-wrap">
		<section className="title-wrap">
			<div className="top-social">
				<SocialIcons />
			</div>
			<div className="bg-top">
				<h1 className="title">{title}</h1>
        {author && <Author author={author} date={date} />}
			</div>
		</section>
	</div>
)

export default BlogHeading
