---
templateKey: 'retreats-page'
path: /retreats
title: Friends Mindful Retreat Mauritius
---
### Join us for the trip of a life time as we travel and retreat to the magnificent island of Mauritius.

This retreat is especially put together for friends and family to have the opportunity to travel, play, explore and retreat together in a fun and rustic way.

The Retreat is set in a pristine Nature Reserve of Mauritius. Immersed in nature and surrounded by rivers and mountains, making it the perfect setting to retreat & explore.

This retreat brings a unique combination of daily yoga and meditation practice, locally curated activities, delicious local foods, adventures and more...

![Mauritius Island View](/assets/images/retreats-mauritius-island-view.jpg)

#### During the course of this retreat, you will experience:

* Daily yoga and meditation practice
* 3 vegetarian meals per day (Optional vegan and pescatarians)
* Island Travels
* Catamaran trip to nearby islands
* Waterfall visits and swims
* Visit to the Mauritius energetically charged Vortex and Sacred Sites
* Snorkeling in marine nature reserve
* Beach Days & coconut sipping
* On Reserve activities include: Kayaking, Mindful Archery, Hiking and more...

![Page Break](/assets/images/retreats-page-break.png)

#### Whats included:

* 7 nights in shared accommodation in beautiful nature reserve
* Airport transfers
* 3 meals per day
* All daily activities and transportation to and from daily trips and activities.

We have sourced the best locally curated deals to put this trip together with so much love and excitement!!!

![Page Break](/assets/images/retreats-page-break-2.png)

#### Retreat Dates are:

1. OCTOBER Friends Retreat 2018
    Friday 19th - Friday 26th

2. DECEMBER Friends Retreat 2018
    Friday 7th - Friday 14th

Bookings and more info with Chloe@shiftza.co.za
