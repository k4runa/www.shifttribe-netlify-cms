import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'
import Content, { HTMLContent } from '../components/Content'

import Social from '../components/Social'
import Tags from '../components/Tags'
import BlogHeading from '../components/BlogHeading'

export const BlogPostTemplate = ({
  content,
  contentComponent,
  author,
  description,
  tags,
  title,
  date,
  helmet,
}) => {
  const PostContent = contentComponent || Content

  return (
    <div className="post-template">
      <div className="site-wrap">
        <section className="post-wrap no-image">
          {helmet || ''}
          <div className="top-wrap">
            <BlogHeading title={title} author={author} date={date} />
          </div>

          <div className="post-content">
            <PostContent content={content} />
            {tags && tags.length ? (<Tags tags={tags} />) : null}
          </div>
          <Social />
        </section>
      </div>
    </div>
  )
}

BlogPostTemplate.propTypes = {
  content: PropTypes.string.isRequired,
  contentComponent: PropTypes.func,
  author: PropTypes.string,
  description: PropTypes.string,
  title: PropTypes.string,
  helmet: PropTypes.instanceOf(Helmet),
}

const BlogPost = ({ data }) => {
  const { markdownRemark: post } = data

  return (
    <BlogPostTemplate
      content={post.html}
      contentComponent={HTMLContent}
      author={post.frontmatter.author}
      date={post.frontmatter.date}
      description={post.frontmatter.description}
      helmet={<Helmet title={`${post.frontmatter.title} | Blog`} />}
      tags={post.frontmatter.tags}
      title={post.frontmatter.title}
    />
  )
}

BlogPost.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object,
  }),
}

export default BlogPost

export const pageQuery = graphql`
  query BlogPostByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        author
        description
        tags
      }
    }
  }
`
