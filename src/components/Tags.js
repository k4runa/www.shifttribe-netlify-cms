import React, { Fragment } from 'react'
import Link from 'gatsby-link'
import { kebabCase } from 'lodash'

const Tags = ({ tags }) => (
	<div className="tags">
		{tags.map((tag, index) => (
			<Fragment>
				{index !== 0 ? <span>&nbsp;/&nbsp;</span> : <span></span> }
				<Link key={tag + `_tag`} to={`/tags/${kebabCase(tag)}/`}>{tag}</Link>
			</Fragment>
		))}
	</div>
)

export default Tags
